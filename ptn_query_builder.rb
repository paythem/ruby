# PayThem.Net API query builder
# Containing response object
# 
# Copyright PayThem.Net WLL, 2014-2020
# 
# Vincent Seaborne vince@paythem.net
# 
# 			WORK IN PROGRESS
class PayThemQueryBuilder 
	# use OpenSSL for RSA encryption
	require 'openssl'
	# use Base64 to encode for tcp transfer
	require 'base64'

	# attributes accessable
	attr_accessor :timezone, :func, :username, :password, :hmac, :initVector, :timestamp, :cipherKey

	# the function to call on the API server
	def func
		@func
	end

	# the username to use for authentication
	def username
		@username
	end

	# the password for the username supplied
	def password
		@password
	end

	# the current client's time zone
	def timezone
		@timezone
	end

	# X-Hash hmac generated from content to be placed in headers
	def hmac
		@hmac
	end

	# initialization vector for cipher
	def initVector
		@initVector
	end

	# the current client's timestamp
	def timestamp
		@timestamp
	end

	# public RSA key used for cipher encryption
	def cipherKey
		@cipherKey
	end

	# checks that the required fields are set
	# some auto generate when not set
	def checkObject
		# username is required. set with 'username' or in method 'setUser'
		if(@username == nil)
			error(1, "No username specified")
		end
		# password is required. set with 'password' or in method 'setUser'
		if(@password == nil)
			error(2, "No password specified")
		end
		# current timezone required. UTC / GMT or in form of the following example "Africa/Harare"
		if(@timezone == nil)
			error(3, "No time zone specified")
		end
		# function on the API server to use. this is required
		if(@func == nil)
			error(4, "No function specified")
		end
		# the current timestamp. when not set, it is set by this class
		if(@timestamp == nil)
			_timestamp = Time.now.to_s
			@timestamp = _timestamp[0.._timestamp.length() - 6]
		end
		# cipher initialization vector. if not set, gets generated and is accessable with 'initVector'
		if(@initVector == nil)
			@initVector = rand(1000000000000000..9999999999999999)
		end
	end

	# new instance passing the cipher key
	def initialize(cipherKey)
		@cipherKey = cipherKey
		@parms = []
		@response = PayThemResponse.new
	end

	# set user details
	def setUser(user, pass)
		@username = user
		@password = pass
	end

	# clear all set parameters
	def clearParameters()
		@parms = []
	end

	# add a parameter to content
	def addParameter(key, value)
		@parms[@parms.length()] = [key, value]
	end

	# create json string for request
	def buildContent
		checkObject()
		if(@response.error)
			return @response
		else
			_jsonString = "{";
			_jsonString += "\"STUB\":\""+rand(1000..99999999).to_s+"\","
			_jsonString += "\"SERVER_TIMEZONE\":\""+@timezone+"\","
			_jsonString += "\"USERNAME\":\""+@username+"\","
			_jsonString += "\"PASSWORD\":\""+@password+"\","
			_jsonString += "\"SERVER_TIMESTAMP\":\""+@timestamp+"\","
			_jsonString += "\"FUNCTION\":\""+@func+"\""
			
			if(@parms.length() > 0)
				_parms = ",\"PARAMETERS\":{";
				_i = 0
				while(_i < @parms.length())
					_parms += "\"" + @parms[_i][0] + "\":\"" + @parms[_i][1] + "\","
					_i += 1
				end
				_parms = _parms[0..(_parms.length() -2)]
				_parms += "}"
				_jsonString += _parms
			end
			_jsonString += "}"
			@parms = []
			_enc = encryptData(_jsonString)
			if(_enc.error)
				return _enc
			else
				success(_enc.content.gsub(/\s+/, ""))
			end
			return @response
		end
	end

	# encrypt generated json string with RSA using cipher key and initialization vector
	def encryptData(stringToEncrypt)
		begin
			cipher = OpenSSL::Cipher.new('AES-256-CBC')
			cipher.encrypt
			cipher.key = @cipherKey
			cipher.iv = @initVector.to_s
			_encrypted = Base64.encode64(cipher.update(stringToEncrypt.to_s) + cipher.final)
			@hmac = OpenSSL::HMAC.hexdigest("SHA256", @cipherKey, stringToEncrypt.to_s)
			success(_encrypted.gsub(/\s+/, ""))
			return @response
		rescue => ex
			error(5, ex.message)
			return @response
		end
	end

	# set an error state
	def error(code, message)
		_resp = PayThemResponse.new
		_resp.error = true
		_resp.errorMessage = message
		@response = _resp
	end

	# set a success state
	def success(message)
		_resp = PayThemResponse.new
		_resp.error = false
		_resp.content = message
		@response = _resp
	end
end

# response handling
class PayThemResponse
	# attributes accessable
	attr_accessor :error, :errorMessage, :errorCode, :content

	# set error state
	def setError(code, message)
		errorCode = code
		errorMessage = message
		error = true
	end

	# set success state
	def setSuccess(resp)
		content = resp
		error = false
	end
end


#Testing custom query building
# require 'net/http'
# qBuilder = PayThemQueryBuilder.new("jbqoxgehynmjydofrbxrtsdocpwzfdcc")
# qBuilder.setUser("U09610", "arcwvvuq")
# qBuilder.func = "get_Voucher"
# qBuilder.timezone = "Africa/johannesburg"
# 
# require 'uri'
# require 'net/http'
# require 'json'
# require "base64"
# content = qBuilder.buildContent().content
# uri = URI('https://vvsq.paythem.net/API/2824/')
# https = Net::HTTP.new(uri.host, uri.port)
# https.use_ssl = true
# request = Net::HTTP::Post.new(uri.path)
# jData = JSON.generate(["test=>some"])
# request['X-Hash'] = qBuilder.hmac
# request['ContentType'] = "application/x-www-form-urlencoded"
# request['X-Public-Key'] = "jbqoxgehynmjydofrbxrtsdocpwzfdcc"
# request.body = URI.encode_www_form({:PUBLIC_KEY => 'jbqoxgehynmjydofrbxrtsdocpwzfdcc', :CONTENT => content })
# response = https.request(request)
# puts response.body